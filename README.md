# OPA Terraform Test

## Steps to run
1. Setup AWS credentials for appropriate region
2. Run `chmod +x deploy.sh`
3. Run `./deploy.sh` and see if the deploy succeeds
4. Delete/uncomment some resources and run `./deploy.sh` again to see the OPA Terraform Test in action

## How it works
> This is mainly focused on preventing `terraform apply` commands which can `delete` one/more resources.

First, we define the `blast_radius = 1` and assign `weights = 1` for each kind of resource deletion events on the `terraform.rego`. 

Then we run the plan and convert the plan output binary to JSON that can be compared against a OPA Policy. 

While comparing the OPA Policy with the resource changes, it increases a counter for every `delete` events and calculates a total score for deletes. So if there is at least one deletion event is happening it won't succeed because of the `score < blast_radius`.
package terraform.analysis

import input as tfplan

########################
# Parameters for Policy
########################
blast_radius = 1

# weights assigned for each operation on each resource-type
weights = {
    "aws_instance": {"delete": 1, "create": 1, "modify": 1},
    "aws_internet_gateway": {"delete": 1, "create": 1, "modify": 1},
    "aws_subnet": {"delete": 1, "create": 1, "modify": 1},
    "aws_route_table": {"delete": 1, "create": 1, "modify": 1},
    "aws_route_table_association": {"delete": 1, "create": 1, "modify": 1},
    "aws_vpc": {"delete": 1, "create": 1, "modify": 1},
    "aws_security_group": {"delete": 1, "create": 1, "modify": 1}
}

# Consider exactly these resource types in calculations
resource_types = {
    "aws_internet_gateway", "aws_instance",
    "aws_subnet", "aws_route_table",
    "aws_route_table_association",
    "aws_vpc","aws_security_group"
}

#########
# Policy
#########

# Authorization holds if score for the plan is acceptable
default fire_in_the_hole = false
fire_in_the_hole {
    score < blast_radius
}

# Compute the score for a Terraform plan as the weighted sum of deletions
score = s {
    all := [ x |
            some resource_type
            crud := weights[resource_type];
            x := crud["delete"] * num_deletes[resource_type]
    ]
    s := sum(all)
}

####################
# Terraform Library
####################

# list of all resources of a given type
resources[resource_type] = all {
    some resource_type
    resource_types[resource_type]
    all := [name |
        name:= tfplan.resource_changes[_]
        name.type == resource_type
    ]
}

# number of deletions of resources of a given type
num_deletes[resource_type] = num {
    some resource_type
    resource_types[resource_type]
    all := resources[resource_type]
    deletions := [res |  res:= all[_]; res.change.actions[_] == "delete"]
    num := count(deletions)
}

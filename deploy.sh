#!/bin/sh

echo "Exporting plans to binary..."
terraform plan --out tfplan.binary

echo "Converting to JSON..."
terraform show -json tfplan.binary > tfplan.json

deploy_score=$(opa eval --format pretty --data terraform.rego --input tfplan.json "data.terraform.analysis.score")
echo "We got score of- ${deploy_score}"

deploy_flag=$(opa eval --format pretty --data terraform.rego --input tfplan.json "data.terraform.analysis.fire_in_the_hole")

if $deploy_flag; then
    echo "All good! Applying..."
    terraform apply -auto-approve "tfplan.binary"
else
    echo "Deploy canceled due to policy issues. Got ${deploy_score} want 0"
fi
